import json
import os
from glob import glob

import semantic_version


def main():
    image_name = os.getenv("IMAGE_NAME")
    version_file = os.path.join(image_name, "version.json")
    with open(version_file) as f:
        data = json.load(f)
    old_version = data["version"]
    new_version = str(semantic_version.Version(old_version).next_patch())
    data["version"] = new_version
    with open(version_file, "w") as f:
        json.dump(data, f)
    print(f"Updating {image_name} version from {old_version} to {new_version}")


if __name__ == "__main__":
    main()
