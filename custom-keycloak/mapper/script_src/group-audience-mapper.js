/**
 * Available variables:
 * user - the current user
 * realm - the current realm
 * token - the current token
 * userSession - the current userSession
 * keycloakSession - the current keycloakSession
 */

// Get user groups
var groups = user.getGroups();
// Iterate groups
groups.forEach(function (group) {
    var attributes = group.getAttributes();

    // Look for 'allowed_audience' attribute
    if (attributes.allowed_audience !== null) {
        var allowed = String(attributes.allowed_audience);
        
        // Check that allowed audiences are correct
        if (allowed !== null && allowed.length > 0) {
            // Remove extra chars and split
            var items = allowed.replace("[", "").replace("]", "").split(",");
            items.forEach(function (item) {
                // Add audience if not empty
                if (
                    item !== null &&
                    item !== undefined &&
                    item !== "undefined" &&
                    item.length > 0) {
                        token.addAudience(item);
                }
            })
        }
    }
});