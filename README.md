# CI Docker Images

This repository and its CI pipeline provide a set of empaia-specific Docker images and are used throughout the EMPAIA project.

## Add a new Docker image

Simply add a new folder (e.g. `new-image`) for the Docker image and provide a `Dockerfile` and `version.json` file in this folder. Data referenced in the `Dockerfile` should also be added to this folder.

The `version.json` file should simply contain a version number:


```json
{
    "version": "0.1.0"
}
```

Afterwards add a new build job to the CI pipeline in `.gitlab-ci.yml`:


```yml
build-new-image:
  extends: .build-docker-image
  variables:
    IMAGE_NAME: new-image
```

Also extend the `automerge-renovate` job in `.gitlab-ci.yml` by:

```yml
    - job: build-new-image
      optional: true
```

The image will then be available at `registry.gitlab.com/empaia/integration/ci-docker-images/new-image:0.1.0`.
